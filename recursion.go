// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package fngo

import (
	"reflect"
)

// Trampoline TODO: NEEDS COMMENT INFO
func Trampoline(in []reflect.Value) []reflect.Value {
	if len(in) < 2 {
		panic("atleast two arguments needed.")
	}

	f := in[0]

	if !IsFunc(f.Interface()) {
		panic("first argument must be a function receiving all args supplied, returning one value.")
	}

	args := in[1:]

	res := reflect.ValueOf(f.Interface()).Call(args)

	if IsFunc(res[0].Interface()) {
		currentFn := reflect.ValueOf(res[0].Interface())
		for {
			val := currentFn.Call([]reflect.Value{})
			if !IsFunc(val[0].Interface()) {
				return []reflect.Value{reflect.ValueOf(val[0].Interface())}
			}
			currentFn = reflect.ValueOf(val[0].Interface())
		}
	}
	return res
}

package fngo

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func isEven(num int64) interface{} {
	if num == 0 {
		return true
	}
	// return isOdd(num - 1)
	return func() interface{} { return isOdd(num - 1) }
}

func isOdd(num int64) interface{} {
	if num == 0 {
		return false
	}
	// return isEven(num - 1)
	return func() interface{} { return isEven(num - 1) }
}

func TestTrampoline(t *testing.T) {

	var evenTramp func(fn interface{}, num int64) bool
	BindFunc(&evenTramp, Trampoline)

	assert.True(t, evenTramp(isEven, 1e6))

	// isEven(1e9)

	var factTramp func(func(int) int, int) int
	var sliceOfFnAndArgs func(input ...interface{}) interface{}
	var fnAndArgs func(fn interface{}, args ...interface{}) interface{}

	BindFunc(&factTramp, Trampoline)
	BindFunc(&sliceOfFnAndArgs, Trampoline)
	BindFunc(&fnAndArgs, Trampoline)

	var factorialFn func(input int) int

	factorialFn = func(input int) int {
		if input == 0 {
			return 1
		}
		return input * factTramp(factorialFn, input-1)
	}

	assert.Equal(t, 1, factorialFn(1))
	assert.Equal(t, 2, factorialFn(2))
	assert.Equal(t, 6, factorialFn(3))
	assert.Equal(t, 24, factorialFn(4))
	assert.Equal(t, 120, factorialFn(5))
	assert.Equal(t, 720, factorialFn(6))
	assert.Equal(t, 5040, factorialFn(7))
	assert.Equal(t, 40320, factorialFn(8))
	assert.Equal(t, 362880, factorialFn(9))
	assert.Equal(t, 3628800, factorialFn(10))

	assert.Panics(t, func() {
		factTramp(nil, 0)
	})

	assert.Panics(t, func() {
		sliceOfFnAndArgs()
	})

	assert.Panics(t, func() {
		fnAndArgs(0, factorialFn)
	})
}

func BenchmarkTrampoline(b *testing.B) {

	var evenTramp func(fn interface{}, num int64) bool
	BindFunc(&evenTramp, Trampoline)

	assert.True(b, evenTramp(isEven, 1e6))

	// isEven(1e9)

	var factTramp func(func(int) int, int) int

	BindFunc(&factTramp, Trampoline)

	var factorialFn func(input int) int

	factorialFn = func(input int) int {
		if input == 0 {
			return 1
		}
		return input * factTramp(factorialFn, input-1)
	}

	b.Run("factorial 5", func(b *testing.B) {
		b.ResetTimer()

		for i := 0; i < b.N; i++ {
			assert.Equal(b, 120, factorialFn(5))
		}
	})

	b.Run("factorial 10", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			assert.Equal(b, 3628800, factorialFn(10))
		}
	})
}

// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package fngo

import (
	"reflect"
	"unsafe"
)

// ensureIfaceVal ensure `reflect.Value` return interface{} value of `i`.
func ensureIfaceVal(i interface{}) interface{} {
	if rv, ok := i.(reflect.Value); ok {
		if rv.IsValid() {
			return rv.Interface()
		}
		return nil
	}
	return i
}

// IsFunc check if args is func.
func IsFunc(maybeFunc interface{}) bool {
	maybeFunc = ensureIfaceVal(maybeFunc)
	if maybeFunc != nil {
		return reflect.TypeOf(maybeFunc).Kind() == reflect.Func
	}
	return false
}

// IsStruct check if arg is struct.
func IsStruct(maybeStruct interface{}) bool {
	maybeStruct = ensureIfaceVal(maybeStruct)
	if maybeStruct != nil {
		if IsPtr(maybeStruct) {
			refVal := reflect.ValueOf(maybeStruct)
			return reflect.Indirect(refVal).Kind() == reflect.Struct
		}
		return reflect.TypeOf(maybeStruct).Kind() == reflect.Struct
	}
	return false
}

// IsPtr check if arg is pointer.
func IsPtr(maybePtr interface{}) bool {
	maybePtr = ensureIfaceVal(maybePtr)
	if maybePtr != nil {
		return reflect.TypeOf(maybePtr).Kind() == reflect.Ptr
	}
	return false
}

// IsMap check if arg is map.
func IsMap(maybeMap interface{}) bool {
	maybeMap = ensureIfaceVal(maybeMap)
	if maybeMap != nil {
		if IsPtr(maybeMap) {
			refVal := reflect.ValueOf(maybeMap)
			return reflect.Indirect(refVal).Kind() == reflect.Map
		}
		return reflect.TypeOf(maybeMap).Kind() == reflect.Map
	}
	return false
}

// IsSlice check if arg is slice.
func IsSlice(maybeSlice interface{}) bool {
	maybeSlice = ensureIfaceVal(maybeSlice)
	if maybeSlice != nil {
		if IsPtr(maybeSlice) {
			refVal := reflect.ValueOf(maybeSlice)
			return reflect.Indirect(refVal).Kind() == reflect.Slice
		}
		return reflect.TypeOf(maybeSlice).Kind() == reflect.Slice
	}
	return false
}

// IsEmpty check if arg is empty either zero by it's type or no entries on structure.
func IsEmpty(arg interface{}) bool {
	arg = ensureIfaceVal(arg)

	if arg != nil {
		switch {
		case IsMap(arg), IsSlice(arg):
			return reflect.ValueOf(arg).Len() < 1
		case IsStruct(arg):
			return reflect.Indirect(reflect.ValueOf(arg)).IsZero()
		default:
			argType := reflect.TypeOf(arg)
			zeroVal := reflect.Zero(argType).Interface()
			return reflect.DeepEqual(arg, zeroVal)
		}
	}
	return true
}

// IsStructComplete check if all field on struct not zero value.
// If not struct return false.
func IsStructComplete(target interface{}) bool {
	if IsStruct(target) {
		v := reflect.ValueOf(target)
		if !IsPtr(target) {
			newAddr := reflect.New(v.Type())
			newAddr.Elem().Set(v)
			target = newAddr.Interface()
		}

		el := reflect.Indirect(reflect.ValueOf(target))
		num := el.NumField()
		for i := 0; i < num; i++ {
			f := el.Field(i)
			if !f.CanSet() {
				/* #nosec G103 */
				f = reflect.NewAt(f.Type(), unsafe.Pointer(f.UnsafeAddr())).Elem()
			}

			if IsEmpty(f.Interface()) {
				return false
			}
		}
	} else {
		return false
	}
	return true
}

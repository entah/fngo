// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package fngo

import (
	"reflect"
)

// Map apply function with each of element of slices provided
// See `gitlab.com/entah/fngo#BindMapFunc`
// Recommended to use `gitlab.com/entah/fngo#BindMapFunc`.
func Map(in []reflect.Value) []reflect.Value {
	f := in[0]
	fn := reflect.ValueOf(f.Interface())
	outType := reflect.TypeOf(f.Interface()).Out(0)
	colls := in[1:]
	index := 0
	res := reflect.MakeSlice(reflect.SliceOf(outType), 0, 0)
	var isExhausted bool

	for {
		args := []reflect.Value{}

		for _, coll := range colls {
			if index >= coll.Len() {
				isExhausted = true
				break
			}
			args = append(args, coll.Index(index))
		}

		if isExhausted {
			break
		}

		index++
		elemVal := fn.Call(args)[0]
		res = reflect.Append(res, elemVal)
	}
	return []reflect.Value{res}
}

// Reduce apply function with/out element of slice
// See `gitlab.com/entah/fngo#BindBindReduceFunc`
// Recommended to use `gitlab.com/entah/fngo#BindReduceFunc`.
func Reduce(in []reflect.Value) []reflect.Value {
	f := in[0]
	fn := reflect.ValueOf(f.Interface())

	if len(in) == 2 {
		coll := in[1]
		collLen := coll.Len()

		switch {
		case collLen < 1:
			elemType := reflect.TypeOf(coll.Interface()).Elem()
			return []reflect.Value{reflect.Zero(elemType)}
		case collLen < 2:
			el := coll.Index(0)
			elType := reflect.TypeOf(coll.Interface()).Elem()
			init := reflect.Zero(elType)
			return fn.Call([]reflect.Value{reflect.ValueOf(init.Interface()), el})
		default:
			init := coll.Index(0)
			for i := 1; i < collLen; i++ {
				init = fn.Call([]reflect.Value{init, coll.Index(i)})[0]
			}
			return []reflect.Value{init}
		}
	}

	init := in[1]
	coll := in[2]
	collLen := coll.Len()

	for i := 0; i < collLen; i++ {
		init = fn.Call([]reflect.Value{init, coll.Index(i)})[0]
	}

	return []reflect.Value{init}
}

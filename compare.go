// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package fngo

import "reflect"

// Or return the first non-empty/non-default value of args
// See `gitlab.com/entah/fngo#BindBindOrFunc`
// Recommended to use `gitlab.com/entah/fngo#BindOrFunc`.
func Or(args []reflect.Value) []reflect.Value {
	a := args[0]

	if !IsSlice(a) {
		panic("func signature must be func(T ...Type) Type")
	}

	switch a.Len() {
	case 0:
		return []reflect.Value{reflect.Zero(a.Type().Elem())}

	case 1:
		if !IsEmpty(a.Index(0)) {
			return []reflect.Value{a.Index(0)}
		}
		return []reflect.Value{reflect.Zero(a.Index(0).Type())}

	case 2:
		res := or(a.Index(0).Interface(), a.Index(1).Interface())
		if IsEmpty(res) {
			return []reflect.Value{reflect.Zero(reflect.TypeOf(res))}
		}
		return []reflect.Value{reflect.ValueOf(res)}

	default:
		d := a.Index(0).Interface()
		l := a.Len()
		i := 1
		for {
			if i < l {
				d = or(d, a.Index(i).Interface())
				i++
				continue
			}

			if IsEmpty(d) {
				return []reflect.Value{reflect.Zero(reflect.TypeOf(d))}
			}
			return []reflect.Value{reflect.ValueOf(d)}
		}
	}
}

// or check if testValue is empty, if empty return defaultValue.
func or(testValue, defaultValue interface{}) interface{} {
	if !IsEmpty(testValue) {
		return testValue
	}
	return defaultValue
}

// ors return the non-default value from args.
func ors(val1, val2 interface{}, values ...interface{}) interface{} {
	if len(values) == 0 {
		return or(val1, val2)
	}
	return ors(or(val1, val2), values[0], values[1:]...)
}

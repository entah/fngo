package fngo

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFilter(t *testing.T) {
	var filterInts1 func(func(int) bool, []int) []int
	BindFilterFunc(&filterInts1)

	assert.Equal(t, []int{1, 2}, filterInts1(func(a int) bool { return a < 3 }, []int{1, 2, 3}))
	assert.Equal(t, []int{}, filterInts1(func(a int) bool { return a < 3 }, []int{}))
	assert.Equal(t, []int{}, filterInts1(func(a int) bool { return a < 3 }, nil))

	assert.Panics(t, func() {
		var fn func(fnAndArgs ...interface{}) interface{}
		BindFilterFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int, int) int, ...interface{}) interface{}
		BindFilterFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int, int) int, interface{}) (interface{}, interface{})
		BindFilterFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int, int), interface{}) interface{}
		BindFilterFunc(fn)
	})

	assert.Panics(t, func() {
		var fn func(int, interface{}) interface{}
		BindFilterFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func() int, interface{}) interface{}
		BindFilterFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int) (int, int), interface{}) interface{}
		BindFilterFunc(&fn)
	})
}

func TestGroupBy(t *testing.T) {
	type person struct {
		name, city string
	}
	var pGroupByCity func(func(person) string, []person) map[string][]person
	BindGroupByFunc(&pGroupByCity)

	pList := []person{{"a", "foo"}, {"b", "foo"}, {"c", "bar"}}
	res := map[string][]person{
		"foo": {{"a", "foo"}, {"b", "foo"}},
		"bar": {{"c", "bar"}},
	}

	assert.Equal(t, res, pGroupByCity(func(p person) string { return p.city }, pList))
	assert.Equal(t, map[string][]person{}, pGroupByCity(func(p person) string { return p.city }, nil))
	assert.Equal(t, map[string][]person{}, pGroupByCity(func(p person) string { return p.city }, []person{}))

	assert.Panics(t, func() {
		var fn func(fnAndArgs ...interface{}) interface{}
		BindGroupByFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int, int) int, ...interface{}) interface{}
		BindGroupByFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int, int) int, interface{}) (interface{}, interface{})
		BindGroupByFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int, int), interface{}) interface{}
		BindGroupByFunc(fn)
	})

	assert.Panics(t, func() {
		var fn func(int, interface{}) interface{}
		BindGroupByFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func() int, interface{}) interface{}
		BindGroupByFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int) (int, int), interface{}) interface{}
		BindGroupByFunc(&fn)
	})
}

func TestIndexBy(t *testing.T) {
	type person struct {
		name, city string
	}
	var pIndexByCity func(func(person) string, []person) map[string]person
	BindIndexByFunc(&pIndexByCity)

	pList := []person{{"a", "foo"}, {"b", "foo"}, {"c", "bar"}}
	res := map[string]person{
		"foo": {"b", "foo"},
		"bar": {"c", "bar"},
	}

	assert.Equal(t, res, pIndexByCity(func(p person) string { return p.city }, pList))
	assert.Equal(t, map[string]person{}, pIndexByCity(func(p person) string { return p.city }, nil))
	assert.Equal(t, map[string]person{}, pIndexByCity(func(p person) string { return p.city }, []person{}))

	assert.Panics(t, func() {
		var fn func(fnAndArgs ...interface{}) interface{}
		BindIndexByFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int, int) int, ...interface{}) interface{}
		BindIndexByFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int, int) int, interface{}) (interface{}, interface{})
		BindIndexByFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int, int), interface{}) interface{}
		BindIndexByFunc(fn)
	})

	assert.Panics(t, func() {
		var fn func(int, interface{}) interface{}
		BindIndexByFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func() int, interface{}) interface{}
		BindIndexByFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int) (int, int), interface{}) interface{}
		BindIndexByFunc(&fn)
	})
}

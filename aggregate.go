// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package fngo

import (
	"reflect"
)

// Filter slice using function returning bool value
// See `gitlab.com/entah/fngo#BindFilterFunc`
// Recommended to use `gitlab.com/entah/fngo#BindFilterFunc`.
func Filter(in []reflect.Value) []reflect.Value {
	f := in[0]
	fn := reflect.ValueOf(f.Interface())
	coll := in[1]
	collLen := coll.Len()
	newColl := reflect.MakeSlice(reflect.TypeOf(coll.Interface()), 0, 0)
	for i := 0; i < collLen; i++ {
		res := fn.Call([]reflect.Value{coll.Index(i)})[0]
		if isTrue, ok := res.Interface().(bool); isTrue && ok {
			newColl = reflect.Append(newColl, coll.Index(i))
		}
	}
	return []reflect.Value{newColl}
}

// GroupBy slice using function returning value for key
// See `gitlab.com/entah/fngo#BindGroupByFunc`
// Recommended to use `gitlab.com/entah/fngo#BindGroupByFunc`.
func GroupBy(in []reflect.Value) []reflect.Value {
	f := in[0]
	fn := reflect.ValueOf(f.Interface())
	coll := in[1]
	collLen := coll.Len()
	collType := reflect.TypeOf(coll.Interface())
	keyType := reflect.TypeOf(f.Interface()).Out(0)
	mapType := reflect.MapOf(keyType, collType)
	newMap := reflect.MakeMap(mapType)

	for i := 0; i < collLen; i++ {
		el := coll.Index(i)
		key := fn.Call([]reflect.Value{el})[0]
		if val := newMap.MapIndex(key); val.IsValid() {
			val = reflect.Append(val, el)
			newMap.SetMapIndex(key, val)
			continue
		}
		newEntry := reflect.Zero(collType)
		newEntry = reflect.Append(newEntry, el)
		newMap.SetMapIndex(key, newEntry)
	}

	reflect.StructOf([]reflect.StructField{})

	return []reflect.Value{newMap}
}

// IndexBy slice using function returning value for key
// See `gitlab.com/entah/fngo#BindIndexByFunc`
// Recommended to use `gitlab.com/entah/fngo#BindIndexByFunc`.
func IndexBy(in []reflect.Value) []reflect.Value {
	f := in[0]
	fn := reflect.ValueOf(f.Interface())
	coll := in[1]
	collLen := coll.Len()
	collType := reflect.TypeOf(coll.Interface())
	keyType := reflect.TypeOf(f.Interface()).Out(0)
	mapType := reflect.MapOf(keyType, collType.Elem())
	newMap := reflect.MakeMap(mapType)

	for i := 0; i < collLen; i++ {
		el := coll.Index(i)
		key := fn.Call([]reflect.Value{el})[0]
		newMap.SetMapIndex(key, el)
	}

	return []reflect.Value{newMap}
}

package fngo

import (
	"errors"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBindOrFunc(t *testing.T) {
	var a func(args ...string) string
	var b func(args ...int) int
	var c func(args ...[]int) []int
	var d func(args ...interface{}) interface{}

	BindOrFunc(&a)
	BindOrFunc(&b)
	BindOrFunc(&c)
	BindOrFunc(&d)

	assert.Equal(t, "foo", a("", "foo"), "string")
	assert.Equal(t, "", a("", ""), "string")
	assert.Equal(t, "foo", a("foo", ""), "string")
	assert.Equal(t, "", a())
	assert.Equal(t, "", a(""), "string")
	assert.Equal(t, "foo", a("foo"), "string")
	assert.Equal(t, "foo", a("", "", "foo"), "string")
	assert.Equal(t, "foo", a("", "", "foo", "", ""), "string")
	assert.Equal(t, "", a("", "", "", "", ""), "string")

	assert.Equal(t, 1, b(0, 1), "int")
	assert.Equal(t, 1, b(1, 0), "int")
	assert.Equal(t, 0, b(0, 0), "int")
	assert.Equal(t, 0, b(), "int")
	assert.Equal(t, 0, b(0), "int")
	assert.Equal(t, 1, b(1), "int")
	assert.Equal(t, 1, b(0, 0, 1), "int")
	assert.Equal(t, 1, b(0, 1, 0), "int")
	assert.Equal(t, 0, b(0, 0, 0), "int")

	assert.Equal(t, []int{1}, c([]int{}, []int{1}), "slice of int")
	assert.Equal(t, []int(nil), c([]int{}, []int{}), "slice of int")
	assert.Equal(t, []int{0}, c([]int{}, []int{0}), "slice of int")
	assert.Equal(t, []int(nil), c(), "slice of int")
	assert.Equal(t, []int(nil), c([]int{}), "slice of int")
	assert.Equal(t, []int{1}, c([]int{1}), "slice of int")
	assert.Equal(t, []int{1}, c([]int{1}, []int{0}, []int{1}), "slice of int")
	assert.Equal(t, []int{0}, c([]int{0}, []int{1}, []int{0}), "slice of int")
	assert.Equal(t, []int{1}, c([]int{}, []int{1}, []int{}), "slice of int")
	assert.Equal(t, []int{1}, c([]int{1}, []int{}, []int{2}), "slice of int")
	assert.Equal(t, []int(nil), c([]int{}, []int{}, []int{}), "slice of int")

	assert.Equal(t, 1, d(0, 1, 2))
	assert.Equal(t, "foo", d(nil, 0, "foo"))

	var x func(a, b interface{}, c ...interface{}) interface{}
	BindFunc(&x, Or)
	assert.Panics(t, func() { x(1, 0, 1) })

	assert.Panics(t, func() {
		var d func(args ...interface{}) (interface{}, interface{})
		BindOrFunc(&d)
	})

	assert.Panics(t, func() {
		BindOrFunc(1)
	})

}

func Test_or(t *testing.T) {
	type args struct {
		testValue    interface{}
		defaultValue interface{}
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		{"test 1", args{testValue: "", defaultValue: "foo"}, "foo"},
		{"test 2", args{testValue: 0, defaultValue: 10}, 10},
		{"test 3", args{testValue: struct{ name string }{}, defaultValue: "foo"}, "foo"},
		{"test 4", args{testValue: nil, defaultValue: 1}, 1},
		{"test 5", args{testValue: 0, defaultValue: 1}, 1},
		{"test 6", args{testValue: 10, defaultValue: 1}, 10},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := or(tt.args.testValue, tt.args.defaultValue); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Fallback() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ors(t *testing.T) {
	type args struct {
		val1   interface{}
		val2   interface{}
		values []interface{}
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		{"1", args{values: []interface{}{1, 0}}, 1},
		{"2", args{values: []interface{}{1, 0, 10}}, 1},
		{"3", args{values: []interface{}{0, false, nil, true}}, true},
		{"3", args{values: []interface{}{0, false, errors.New("test"), true}}, errors.New("test")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ors(tt.args.val1, tt.args.val2, tt.args.values...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ors() = %v, want %v", got, tt.want)
			}
		})
	}
}

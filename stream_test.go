package fngo

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMap(t *testing.T) {
	var mapIntcs3 func(fn func(el ...int) int, coll1, coll2, coll3 []int) []int
	var mapIntcs1 func(fn func(el int) int, coll1 []int) []int
	// var mapIntcs1IfaceFn func(fn interface{}, coll1 []interface{}) []int

	BindMapFunc(&mapIntcs3)
	BindMapFunc(&mapIntcs1)

	sum := func(ints ...int) int {
		res := 0
		for _, i := range ints {
			res += i
		}
		return res
	}

	inc := func(i int) int {
		return i + 1
	}

	assert.Equal(t, []int{6, 9, 12}, mapIntcs3(sum, []int{1, 2, 3}, []int{2, 3, 4}, []int{3, 4, 5}))
	assert.Equal(t, []int{6, 9}, mapIntcs3(sum, []int{1, 2, 3}, []int{2, 3, 4}, []int{3, 4}))
	assert.Equal(t, []int{2, 3, 4}, mapIntcs1(inc, []int{1, 2, 3}))
	assert.Equal(t, []int{}, mapIntcs1(inc, []int{}))
	assert.Equal(t, []int{}, mapIntcs3(sum, []int{1, 2, 3}, []int{2, 3, 4}, []int{}))
	assert.Equal(t, []int{}, mapIntcs3(sum, []int{1, 2, 3}, []int{2, 3, 4}, nil))
	assert.Equal(t, []int{}, mapIntcs1(inc, nil))

	assert.Panics(t, func() {
		var fn func(fnAndArgs ...interface{}) interface{}
		BindMapFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(fn interface{}, args ...interface{}) interface{}
		BindMapFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(fn interface{}, args interface{}) (interface{}, interface{})
		BindMapFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(fn interface{}, args interface{}) interface{}
		BindMapFunc(fn)
	})

	assert.Panics(t, func() {
		var fn func(fn func(int) (int, int), args interface{}) interface{}
		BindMapFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(int, interface{}, interface{}) interface{}
		BindMapFunc(&fn)
	})
}

func TestReduce(t *testing.T) {
	var reduceInts1 func(func(init, next int) int, []int) int
	BindReduceFunc(&reduceInts1)

	assert.Equal(t, 6, reduceInts1(func(init, next int) int { return init + next }, []int{1, 2, 3}))
	assert.Equal(t, 1, reduceInts1(func(init, next int) int { return init + next }, []int{1}))
	assert.Equal(t, 0, reduceInts1(func(init, next int) int { return init + next }, []int{}))
	assert.Equal(t, 0, reduceInts1(func(init, next int) int { return init + next }, nil))

	var reduceInts2 func(func(init, next int) int, int, []int) int
	BindReduceFunc(&reduceInts2)

	assert.Equal(t, 7, reduceInts2(func(init, next int) int { return init + next }, 1, []int{1, 2, 3}))
	assert.Equal(t, 2, reduceInts2(func(init, next int) int { return init + next }, 1, []int{1}))
	assert.Equal(t, 1, reduceInts2(func(init, next int) int { return init + next }, 1, nil))

	assert.Panics(t, func() {
		var fn func(fnAndArgs ...interface{}) interface{}
		BindReduceFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int, int) int, ...interface{}) interface{}
		BindReduceFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int, int) int, interface{}) (interface{}, interface{})
		BindReduceFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int, int), interface{}) interface{}
		BindReduceFunc(fn)
	})

	assert.Panics(t, func() {
		var fn func(int, interface{}) interface{}
		BindReduceFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func() int, interface{}) interface{}
		BindReduceFunc(&fn)
	})

	assert.Panics(t, func() {
		var fn func(func(int, int) (int, int), interface{}) interface{}
		BindReduceFunc(&fn)
	})
}

type ReflectFunc func([]reflect.Value) []reflect.Value

func Partial(fn interface{}, args ...interface{}) ReflectFunc {
	var toReflectArgs func(func(interface{}) reflect.Value, []interface{}) []reflect.Value
	BindMapFunc(&toReflectArgs)
	firstArgs := toReflectArgs(reflect.ValueOf, args)
	f := reflect.ValueOf(fn)

	return func(rest []reflect.Value) []reflect.Value {
		collArgs := rest[0]
		collLen := collArgs.Len()
		for i := 0; i < collLen; i++ {
			firstArgs = append(firstArgs, collArgs.Index(i))
		}
		return f.Call(firstArgs)
	}
}

func Apply(fn interface{}, args ...interface{}) ReflectFunc {
	var toReflectArgs func(func(interface{}) reflect.Value, []interface{}) []reflect.Value
	BindMapFunc(&toReflectArgs)
	firstArgs := toReflectArgs(reflect.ValueOf, args)
	f := reflect.ValueOf(fn)

	return func(rest []reflect.Value) []reflect.Value {
		collArgs := rest[0]
		collLen := collArgs.Len()
		for i := 0; i < collLen; i++ {
			firstArgs = append(firstArgs, collArgs.Index(i))
		}
		return f.Call(firstArgs)
	}
}

// type Attr interface{}
// type Nested  map[Attr]
// type Facts []Attr

func TestReflectClosure(t *testing.T) {
	var reduce func(func(int, int) int, []int) int
	BindReduceFunc(&reduce)

	f := func(ints ...int) int {
		return reduce(func(i1, i2 int) int { return i1 + i2 }, ints)
	}

	var ff func(...int) int
	BindFunc(&ff, Partial(f, 1))

	assert.Equal(t, 6, ff(2, 3))

	type fx func(...[]int) int

	var fxx fx

	// var fff func(...[]int) int
	BindFunc(&fxx, Partial(reduce, func(i1, i2 int) int { return i1 + i2 }))
	assert.Equal(t, 6, fxx([]int{1, 2, 3}))

	// foo := Person{
	// 	Name: "Foo",
	// 	Age:  17,
	// 	City: "a",
	// }

	// bar := Person{
	// 	Name: "Bar",
	// 	Age:  16,
	// 	City: "a",
	// }

	// foobar := Person{
	// 	Name: "FooBar",
	// 	Age:  21,
	// 	City: "b",
	// 	// Friends: foo,
	// 	Friends: []Person{foo, bar},
	// }

	// friends := []Person{foo, bar, foobar}

	// query := []interface{}{
	// 	"Name",
	// 	"Friends",
	// 	// map[interface{}][]interface{}{
	// 	// 	"Friends": {
	// 	// 		"Name",
	// 	// 	},
	// 	// },
	// }

	// strct := reflect.ValueOf(foobar)
	// qry := reflect.ValueOf(query)

	// for _, i :=

	// fmt.Printf("%+v", fromKey(qry, strct).Interface())
	// t.Fail()

	// buff, marshalErr := json.Marshal(iterateStruct(qry, strct).Interface())
	// assert.NoError(t, marshalErr)
	// assert.Equal(
	// 	t,
	// 	`{"Name":"FooBar","Friends":[{"Name":"Foo"},{"Name":"Bar"}]}`,
	// 	string(buff),
	// )

	// strct2 := reflect.ValueOf(friends)
	// buff2, marshalErr2 := json.Marshal(iterateStruct(qry, strct2).Interface())
	// assert.NoError(t, marshalErr2)
	// assert.Equal(
	// 	t,
	// 	`[{"Name":"Foo","Friends":[]},{"Name":"Bar","Friends":[]},{"Name":"FooBar","Friends":[{"Name":"Foo"},{"Name":"Bar"}]}]`,
	// 	string(buff2),
	// )

	// strct3 := reflect.ValueOf([]Person{})
	// buff3, marshalErr3 := json.Marshal(iterateStruct(qry, strct3).Interface())
	// assert.NoError(t, marshalErr3)
	// assert.Equal(
	// 	t,
	// 	`[]`,
	// 	string(buff3),
	// )

	// strct4 := reflect.ValueOf([]Person(nil))
	// buff4, marshalErr4 := json.Marshal(iterateStruct(qry, strct4).Interface())
	// assert.NoError(t, marshalErr4)
	// assert.Equal(
	// 	t,
	// 	`[]`,
	// 	string(buff4),
	// )
}

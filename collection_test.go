package fngo

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFirst(t *testing.T) {
	var firstOfInts func([]int) int
	BindFunc(&firstOfInts, First)

	assert.Equal(t, 1, firstOfInts([]int{1, 2, 3, 4}))
	assert.Equal(t, 100, firstOfInts([]int{100, 99, 10, 8}))
	assert.Equal(t, 0, firstOfInts(nil))
	assert.Equal(t, 0, firstOfInts([]int{}))

	var firstOfIfaces func([]interface{}) interface{}
	BindFunc(&firstOfIfaces, First)
	assert.Equal(t, 1, firstOfIfaces([]interface{}{1, 2, 3, 4}))
	assert.Equal(t, nil, firstOfIfaces(nil))

	var firstOfString func(string) string
	BindFunc(&firstOfString, First)

	assert.Equal(t, "f", firstOfString("foobar"))
	assert.Equal(t, "", firstOfString(""))

	var firstOfMap func(map[string]string) (string, string)

	BindFunc(&firstOfMap, First)
	s1, s2 := firstOfMap(map[string]string{"foo": "bar"})
	assert.Equal(t, []string{"foo", "bar"}, []string{s1, s2})
	s3, s4 := firstOfMap(nil)
	assert.Equal(t, []string{"", ""}, []string{s3, s4})

	type person struct {
		FName string
		LName string
	}

	var firstOfStruct func(person) (string, string)
	BindFunc(&firstOfStruct, First)
	k1, v1 := firstOfStruct(person{FName: "FOO"})
	assert.Equal(t, []string{"FName", "FOO"}, []string{k1, v1})

	var varArgs func(...interface{}) []interface{}
	BindFunc(&varArgs, First)

	assert.Panics(t, func() {
		varArgs(0, 1)
	})

	var nonCollection func(interface{}) interface{}
	BindFunc(&nonCollection, First)

	assert.Panics(t, func() {
		nonCollection(0)
	})
}

func TestLast(t *testing.T) {
	var lastOfInts func([]int) int
	BindFunc(&lastOfInts, Last)

	assert.Equal(t, 4, lastOfInts([]int{1, 2, 3, 4}))
	assert.Equal(t, 8, lastOfInts([]int{100, 99, 10, 8}))
	assert.Equal(t, 0, lastOfInts(nil))
	assert.Equal(t, 0, lastOfInts([]int{}))

	var lastOfIfaces func([]interface{}) interface{}
	BindFunc(&lastOfIfaces, Last)
	assert.Equal(t, 4, lastOfIfaces([]interface{}{1, 2, 3, 4}))
	assert.Equal(t, nil, lastOfIfaces(nil))

	var lastOfString func(string) string
	BindFunc(&lastOfString, Last)

	assert.Equal(t, "r", lastOfString("foobar"))
	assert.Equal(t, "", lastOfString(""))

	var lastOfMap func(map[string]string) (string, string)

	BindFunc(&lastOfMap, Last)
	s1, s2 := lastOfMap(map[string]string{"foo": "bar"})
	assert.Equal(t, []string{"foo", "bar"}, []string{s1, s2})
	s3, s4 := lastOfMap(nil)
	assert.Equal(t, []string{"", ""}, []string{s3, s4})

	type person struct {
		FName string
		LName string
	}

	var lastOfStruct func(person) (string, string)
	BindFunc(&lastOfStruct, Last)
	k1, v1 := lastOfStruct(person{LName: "BAR"})
	assert.Equal(t, []string{"LName", "BAR"}, []string{k1, v1})

	var varArgs func(...interface{}) []interface{}
	BindFunc(&varArgs, Last)

	assert.Panics(t, func() {
		varArgs(0, 1)
	})

	var nonCollection func(interface{}) interface{}
	BindFunc(&nonCollection, Last)

	assert.Panics(t, func() {
		nonCollection(0)
	})
}

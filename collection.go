// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package fngo

import "reflect"

// First TODO: NEEDS COMMENT INFO
func First(in []reflect.Value) []reflect.Value {

	coll := in[0]
	collVal := coll.Interface()

	switch {
	case IsSlice(collVal):
		if coll.Len() < 1 {
			return []reflect.Value{reflect.Zero(reflect.TypeOf(collVal).Elem())}
		}
		return []reflect.Value{coll.Index(0)}

	case reflect.TypeOf(collVal).Kind() == reflect.String:
		str := collVal.(string)
		if len(str) < 1 {
			return []reflect.Value{reflect.ValueOf("")}
		}
		return []reflect.Value{reflect.ValueOf(string(str[0]))}

	case IsMap(collVal):
		keys := coll.MapKeys()
		if len(keys) < 1 {
			keyType := reflect.TypeOf(collVal).Key()
			valType := reflect.TypeOf(collVal).Elem()
			return []reflect.Value{reflect.Zero(keyType), reflect.Zero(valType)}
		}
		key := keys[0]
		val := coll.MapIndex(key)
		return []reflect.Value{key, val}

	case IsStruct(collVal):
		val := coll.Field(0)
		key := coll.Type().Field(0).Name
		return []reflect.Value{reflect.ValueOf(key), val}

	default:
		panic("argument must one of associative type")

	}
}

// Last TODO: NEEDS COMMENT INFO
func Last(in []reflect.Value) []reflect.Value {
	coll := in[0]
	collVal := coll.Interface()

	switch {
	case IsSlice(collVal):
		if coll.Len() < 1 {
			return []reflect.Value{reflect.Zero(reflect.TypeOf(collVal).Elem())}
		}
		return []reflect.Value{coll.Index(coll.Len() - 1)}

	case reflect.TypeOf(collVal).Kind() == reflect.String:
		str := collVal.(string)
		if len(str) < 1 {
			return []reflect.Value{reflect.ValueOf("")}
		}
		return []reflect.Value{reflect.ValueOf(string(str[len(str)-1]))}

	case IsMap(collVal):
		keys := coll.MapKeys()
		if len(keys) < 1 {
			keyType := reflect.TypeOf(collVal).Key()
			valType := reflect.TypeOf(collVal).Elem()
			return []reflect.Value{reflect.Zero(keyType), reflect.Zero(valType)}
		}
		key := keys[len(keys)-1]
		val := coll.MapIndex(key)
		return []reflect.Value{key, val}

	case IsStruct(collVal):
		lenStruct := coll.NumField()
		val := coll.Field(lenStruct - 1)
		key := coll.Type().Field(lenStruct - 1).Name
		return []reflect.Value{reflect.ValueOf(key), val}

	default:
		panic("argument must one of associative type")

	}
}

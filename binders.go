// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

package fngo

import (
	"fmt"
	"reflect"
)

// BindFunc binding func via  `reflect.MakeFunc`.
// `i` is pointer of the func want to bind.
// `f` is layout needed by `reflect.MakeFunc`.
func BindFunc(i interface{}, f func(args []reflect.Value) []reflect.Value) {
	impl := reflect.ValueOf(i).Elem()
	impl.Set(reflect.MakeFunc(impl.Type(), f))
}

// BindMapFunc bind pointer of map function to `gitlab.com/entah/fngo#Map`
// The map function atleast take 2 arguments
// First argument must be function with argument of each element of slice supplied,
// Second, third, ..., nth argument are slices, not accept variadic argument
// Stop when one of slices exhausted
// Always return slice of function return type.
func BindMapFunc(mapFunc interface{}) {
	rv := reflect.ValueOf(mapFunc)
	irv := reflect.Indirect(rv)
	rt := reflect.TypeOf(irv.Interface())

	if !(rt.Kind() == reflect.Func && IsPtr(mapFunc)) {
		panic(fmt.Sprintf("arg must be a pointer of a function, got: %T", mapFunc))
	}

	isValidInput := rt.NumIn() > 1
	isValidOutput := rt.NumOut() == 1
	isValidArgs := !rt.IsVariadic()
	innerFn := rt.In(0)
	isValid1stArg := innerFn.Kind() == reflect.Func

	switch {
	case !isValidInput && !isValidArgs:
		panic("function arguments must at least 2 argument function and collection\n or func, coll1, coll2, ...colln, and not as variadic arguments.")
	case !isValidOutput:
		panic(fmt.Sprintf("function must return 1 value, your function return %d", rt.NumOut()))
	case !isValidArgs:
		panic("function args must not contains variadic argument.")
	case !isValid1stArg:
		panic("first argument must be a function take each element of colls as arguments, returning one value.")
	case innerFn.NumOut() != 1:
		panic("inner func return value must only one value")
	default:
		BindFunc(mapFunc, Map)
	}
}

// BindReduceFunc bind pointer of reduce function to `gitlab.com/entah/fngo#Reduce`
// The reduce function either take 2 or 3 arguments
// If function with 2 argument, function will receive 1st and 2nd elem of slice of 2nd argument
// If function with 3 argument, function will receive 2nd argument as init value and element of slice of 3rd argument.
func BindReduceFunc(reduceFunc interface{}) {
	rv := reflect.ValueOf(reduceFunc)
	irv := reflect.Indirect(rv)
	rt := reflect.TypeOf(irv.Interface())

	if !(rt.Kind() == reflect.Func && IsPtr(reduceFunc)) {
		panic(fmt.Sprintf("arg must be a pointer of a function, got: %T", reduceFunc))
	}

	isValidInput := rt.NumIn() == 2 || rt.NumIn() == 3
	isValidOutput := rt.NumOut() == 1
	isValidArgs := !rt.IsVariadic()
	innerFn := rt.In(0)
	isValid1stArg := innerFn.Kind() == reflect.Func

	switch {
	case !isValidInput && !isValidArgs:
		panic("function arguments must at least 2 argument function and collection\n or func, init, and coll and not as variadic arguments.")
	case !isValidOutput:
		panic(fmt.Sprintf("function must return 1 value, your function return %d", rt.NumOut()))
	case !isValidArgs:
		panic("function args must not contains variadic argument.")
	case !isValid1stArg:
		panic("first argument must be a function take 2 arguments, returning one value.")
	case innerFn.NumIn() != 2:
		panic("inner function must take 2 arguments.")
	case innerFn.NumOut() != 1:
		panic("inner func return value must only one value")
	default:
		BindFunc(reduceFunc, Reduce)
	}
}

// BindOrFunc bind pointer of or function to `gitlab.com/entah/fngo#Or`
// The or function takes variadic args and return the first non empty/default value from right to left.
func BindOrFunc(orFunc interface{}) {
	rv := reflect.ValueOf(orFunc)
	irv := reflect.Indirect(rv)
	rt := reflect.TypeOf(irv.Interface())

	if !(rt.Kind() == reflect.Func && IsPtr(orFunc)) {
		panic(fmt.Sprintf("arg must be a pointer of a function, got: %T", orFunc))
	}

	if rt.NumOut() != 1 {
		panic(fmt.Sprintf("function must return 1 value, your function return %d", rt.NumOut()))
	}

	impl := reflect.ValueOf(orFunc).Elem()
	impl.Set(reflect.MakeFunc(impl.Type(), Or))
}

// BindFilterFunc bind pointer of filter function to `gitlab.com/entah/fngo#Filter`
// The filter function take 1 argument and return boolean value.
func BindFilterFunc(filterFunc interface{}) {
	rv := reflect.ValueOf(filterFunc)
	irv := reflect.Indirect(rv)
	rt := reflect.TypeOf(irv.Interface())

	if !(rt.Kind() == reflect.Func && IsPtr(filterFunc)) {
		panic(fmt.Sprintf("arg must be a pointer of a function, got: %T", filterFunc))
	}

	isValidInput := rt.NumIn() == 2
	isValidOutput := rt.NumOut() == 1
	isValidArgs := !rt.IsVariadic()
	innerFn := rt.In(0)
	isValid1stArg := innerFn.Kind() == reflect.Func

	switch {
	case !isValidInput && !isValidArgs:
		panic("function arguments must take 2 argument function and collection.")
	case !isValidOutput:
		panic(fmt.Sprintf("function must return 1 value, your function return %d", rt.NumOut()))
	case !isValidArgs:
		panic("function args must not contains variadic argument.")
	case !isValid1stArg:
		panic("first argument must be a function take 1 argument, returning boolean value.")
	case innerFn.NumIn() != 1:
		panic("inner function must take 1 arguments.")
	case innerFn.NumOut() != 1 || innerFn.Out(0).Kind() != reflect.Bool:
		panic("inner func return value must only one value and kind of boolean value.")
	default:
		BindFunc(filterFunc, Filter)
	}
}

// BindGroupByFunc bind pointer of groupBy function to `gitlab.com/entah/fngo#GroupBy`
// The groupBy function take 1 argument and return a value for a map key.
func BindGroupByFunc(groupByFunc interface{}) {
	rv := reflect.ValueOf(groupByFunc)
	irv := reflect.Indirect(rv)
	rt := reflect.TypeOf(irv.Interface())

	if !(rt.Kind() == reflect.Func && IsPtr(groupByFunc)) {
		panic(fmt.Sprintf("arg must be a pointer of a function, got: %T", groupByFunc))
	}

	isValidInput := rt.NumIn() == 2
	isValidOutput := rt.NumOut() == 1
	isValidArgs := !rt.IsVariadic()
	innerFn := rt.In(0)
	isValid1stArg := innerFn.Kind() == reflect.Func

	switch {
	case !isValidInput && !isValidArgs:
		panic("function arguments must take 2 argument function and collection.")
	case !isValidOutput:
		panic(fmt.Sprintf("function must return 1 value, your function return %d", rt.NumOut()))
	case !isValidArgs:
		panic("function args must not contains variadic argument.")
	case !isValid1stArg:
		panic("first argument must be a function take 1 argument, returning boolean value.")
	case innerFn.NumIn() != 1:
		panic("inner function must take 1 arguments.")
	case innerFn.NumOut() != 1:
		panic("inner func return value must only one value.")
	default:
		BindFunc(groupByFunc, GroupBy)
	}
}

// BindIndexByFunc bind pointer of indexBy function to `gitlab.com/entah/fngo#IndexBy`
// The indexBy function take 1 argument and return a value for a map key
// Unlike  `gitlab.com/entah/fngo#GroupBy`, map does not have a slice of `T` but one value each key
// The latest key will win.
func BindIndexByFunc(indexByFunc interface{}) {
	rv := reflect.ValueOf(indexByFunc)
	irv := reflect.Indirect(rv)
	rt := reflect.TypeOf(irv.Interface())

	if !(rt.Kind() == reflect.Func && IsPtr(indexByFunc)) {
		panic(fmt.Sprintf("arg must be a pointer of a function, got: %T", indexByFunc))
	}

	isValidInput := rt.NumIn() == 2
	isValidOutput := rt.NumOut() == 1
	isValidArgs := !rt.IsVariadic()
	innerFn := rt.In(0)
	isValid1stArg := innerFn.Kind() == reflect.Func

	switch {
	case !isValidInput && !isValidArgs:
		panic("function arguments must take 2 argument function and collection.")
	case !isValidOutput:
		panic(fmt.Sprintf("function must return 1 value, your function return %d", rt.NumOut()))
	case !isValidArgs:
		panic("function args must not contains variadic argument.")
	case !isValid1stArg:
		panic("first argument must be a function take 1 argument, returning boolean value.")
	case innerFn.NumIn() != 1:
		panic("inner function must take 1 arguments.")
	case innerFn.NumOut() != 1:
		panic("inner func return value must only one value.")
	default:
		BindFunc(indexByFunc, IndexBy)
	}
}

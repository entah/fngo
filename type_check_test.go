package fngo

import (
	"reflect"
	"testing"
	"time"
)

func TestIsEmpty(t *testing.T) {
	var emptyTime time.Time
	rfEmptyStr := reflect.ValueOf("")
	rfNil := reflect.ValueOf(nil)
	rfZeroNumber := reflect.ValueOf(0)
	rfStr := reflect.ValueOf("foo")
	rfNumber := reflect.ValueOf(1234)
	intVar := 1
	stringVar := "foo"
	zeroVar := 0
	emptyString := ""
	type args struct {
		arg interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"test 1", args{arg: nil}, true},
		{"test 2", args{arg: false}, true},
		{"test 3", args{arg: true}, false},
		{"test 4", args{arg: 0}, true},
		{"test 5", args{arg: 1}, false},
		{"test 6", args{arg: &struct{ name string }{"foo"}}, false},
		{"test 7", args{arg: &struct{ name string }{}}, true},
		{"test 8", args{arg: struct{ name string }{"foo"}}, false},
		{"test 9", args{arg: struct{ name string }{}}, true},
		{"test 10", args{arg: ""}, true},
		{"test 11", args{arg: "foo"}, false},
		{"test 12", args{arg: []string{"foo"}}, false},
		{"test 13", args{arg: []string{}}, true},
		{"test 14", args{arg: &intVar}, false},
		{"test 15", args{arg: &stringVar}, false},
		{"test 16", args{arg: &zeroVar}, false},
		{"test 17", args{arg: &emptyString}, false},
		{"test 18", args{arg: struct{ Name struct{ First string } }{}}, true},
		{"test 19", args{arg: &struct{ Name struct{ First string } }{}}, true},
		{"test 20", args{arg: struct{ Name struct{ First string } }{struct{ First string }{"foo"}}}, false},
		{"test 21", args{arg: &struct{ Name struct{ First string } }{struct{ First string }{"foo"}}}, false},
		{"test 22", args{arg: rfEmptyStr}, true},
		{"test 23", args{arg: rfNil}, true},
		{"test 24", args{arg: rfZeroNumber}, true},
		{"test 25", args{arg: rfStr}, false},
		{"test 26", args{arg: rfNumber}, false},
		{"test 27", args{arg: emptyTime}, true},
		{"test 28", args{arg: time.Time{}}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsEmpty(tt.args.arg); got != tt.want {
				t.Errorf("IsEmpty() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsFunc(t *testing.T) {
	type args struct {
		maybeFunc interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"test 1", args{maybeFunc: nil}, false},
		{"test 2", args{maybeFunc: "foo"}, false},
		{"test 3", args{maybeFunc: 1}, false},
		{"test 4", args{maybeFunc: func() {}}, true},
		{"test 5", args{maybeFunc: func() string { return "foo" }}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsFunc(tt.args.maybeFunc); got != tt.want {
				t.Errorf("IsFunc() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsPtr(t *testing.T) {
	type args struct {
		maybePtr interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"test 1", args{maybePtr: nil}, false},
		{"test 2", args{maybePtr: "foo"}, false},
		{"test 3", args{maybePtr: 1}, false},
		{"test 4", args{maybePtr: func() {}}, false},
		{"test 5", args{maybePtr: func() string { return "foo" }}, false},
		{"test 6", args{maybePtr: &struct{ Name string }{}}, true},
		{"test 7", args{maybePtr: &map[string]int{"a": 1}}, true},
		{"test 8", args{maybePtr: reflect.ValueOf((error)(nil))}, false},
		{"test 9", args{maybePtr: reflect.ValueOf(&map[string]int{"a": 1})}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsPtr(tt.args.maybePtr); got != tt.want {
				t.Errorf("IsPtr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsStructComplete(t *testing.T) {
	type args struct {
		target interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			"test 1",
			args{target: struct{ Name string }{}},
			false,
		},
		{
			"test 2",
			args{target: 1},
			false,
		},
		{
			"test 3",
			args{target: struct{ Name string }{"foo"}},
			true,
		},
		{
			"test 4",
			args{target: struct {
				Name string
				age  int
			}{"foo", 0}},
			false,
		},
		{
			"test 5",
			args{target: &struct {
				Name string
				age  int
			}{"foo", 0}},
			false,
		},
		{
			"test 6",
			args{target: &struct {
				Name string
				age  int
			}{"foo", 1}},
			true,
		},
		{
			"test 7",
			args{target: &struct {
				Name string
				age  int
			}{"", 1}},
			false,
		},
		{
			"test 8",
			args{target: nil},
			false,
		},
		{
			"test 9",
			args{target: &struct {
				Name string
				do   func()
			}{"bar", nil}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsStructComplete(tt.args.target); got != tt.want {
				t.Errorf("IsComplete() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsMap(t *testing.T) {
	type args struct {
		maybeMap interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"test 1", args{maybeMap: nil}, false},
		{"test 2", args{maybeMap: "foo"}, false},
		{"test 3", args{maybeMap: 1}, false},
		{"test 4", args{maybeMap: func() {}}, false},
		{"test 5", args{maybeMap: func() string { return "foo" }}, false},
		{"test 6", args{maybeMap: map[string]bool{}}, true},
		{"test 7", args{maybeMap: []string{}}, false},
		{"test 8", args{maybeMap: &map[string]bool{}}, true},
		{"test 9", args{maybeMap: reflect.ValueOf((error)(nil))}, false},
		{"test 10", args{maybeMap: reflect.ValueOf(&map[string]int{"a": 1})}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsMap(tt.args.maybeMap); got != tt.want {
				t.Errorf("IsMap() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsSlice(t *testing.T) {
	type args struct {
		maybeSlice interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"test 1", args{maybeSlice: nil}, false},
		{"test 2", args{maybeSlice: "foo"}, false},
		{"test 3", args{maybeSlice: 1}, false},
		{"test 4", args{maybeSlice: func() {}}, false},
		{"test 5", args{maybeSlice: func() string { return "foo" }}, false},
		{"test 6", args{maybeSlice: map[string]bool{}}, false},
		{"test 7", args{maybeSlice: []string{}}, true},
		{"test 8", args{maybeSlice: &map[string]bool{}}, false},
		{"test 9", args{maybeSlice: reflect.ValueOf((error)(nil))}, false},
		{"test 10", args{maybeSlice: reflect.ValueOf(&map[string]int{"a": 1})}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsSlice(tt.args.maybeSlice); got != tt.want {
				t.Errorf("IsSlice() = %v, want %v", got, tt.want)
			}
		})
	}
}
